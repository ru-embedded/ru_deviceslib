/*
 *  hd44780.c
 *
 *  Created on: 2020-01-05
 *  Author: rafau
 */

#include "hd44780.h"

#include <stdio.h>
#include <stdlib.h>

#define HD44780_CLEAR_DISP     0x01
#define HD44780_RETURN_HOME    0x02
#define HD44780_SET_CGRAM_ADDR 0x40
#define HD44780_SET_DDRAM_ADDR 0x80

#define HD44780_ENTRY              0x04
#define HD44780_ENTRY_SHIFT_CURSOR 0x01
#define HD44780_ENTRY_SHIFT_DISP   0x00
#define HD44780_ENTRY_DECREMENT    0x00
#define HD44780_ENTRY_INCREMENT    0x02

#define HD44780_DISP_CONTROL 0x08
#define HD44780_DISP_OFF     0x00
#define HD44780_DISP_ON      0x04
#define HD44780_CURSOR_OFF   0x00
#define HD44780_CURSOR_ON    0x02
#define HD44780_BLINK_OFF    0x00
#define HD44780_BLINK_ON     0x01

#define HD44780_SHIFT        0x10
#define HD44780_SHIFT_CURSOR 0x00
#define HD44780_SHIFT_DISP   0x08
#define HD44780_SHIFT_LEFT   0x00
#define HD44780_SHIFT_RIGHT  0x04

#define HD44780_FUNC_SET  0x20
#define HD44780_8BIT_MODE 0x10
#define HD44780_4BIT_MODE 0x00
#define HD44780_2LINE     0x08
#define HD44780_1LINE     0x00
#define HD44780_5x10_DOTS 0x04
#define HD44780_5x8_DOTS  0x00

static hd44780_error_t _device_check(struct hd44780_device *dev) {
    if (!dev) return hd44780_error_NODEV;
    if (!dev->driver) return hd44780_error_NODRV;
    if (!dev->driver->rs_pin_set_func || !dev->driver->rw_pin_set_func || !dev->driver->e_pin_set_func ||
        !dev->driver->data_set_func || !dev->driver->delay_ms_func)
        return hd44780_error_NODRVF;
    return hd44780_error_SUCCESS;
}

static void _lcd_send(struct hd44780_device *dev, uint8_t data, uint8_t mode) {
    dev->driver->rw_pin_set_func(0);

    dev->driver->rs_pin_set_func(mode);
    dev->driver->e_pin_set_func(1);
    dev->driver->data_set_func(data);
    dev->driver->e_pin_set_func(0);
    dev->driver->delay_ms_func(1);
}

static inline void _lcd_data(struct hd44780_device *dev, uint8_t data) {
    _lcd_send(dev, data, 1);
}

static inline void _lcd_command(struct hd44780_device *dev, uint8_t data) {
    _lcd_send(dev, data, 0);
}

uint8_t hd44780_init(struct hd44780_device *dev) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->driver->delay_ms_func(15);

    dev->driver->rs_pin_set_func(0);
    dev->driver->e_pin_set_func(0);
    dev->driver->rw_pin_set_func(0);

    _lcd_data(dev, 0x30);
    dev->driver->delay_ms_func(5);

    _lcd_data(dev, 0x30);
    dev->driver->delay_ms_func(5);

    _lcd_data(dev, 0x30);
    dev->driver->delay_ms_func(5);

    _lcd_command(dev, HD44780_FUNC_SET | HD44780_5x8_DOTS | HD44780_2LINE | HD44780_8BIT_MODE);
    _lcd_command(dev, HD44780_DISP_CONTROL | HD44780_DISP_OFF);
    _lcd_command(dev, HD44780_CLEAR_DISP);
    dev->driver->delay_ms_func(2);
    _lcd_command(dev, HD44780_ENTRY | HD44780_ENTRY_INCREMENT);
    dev->lcd_control = HD44780_CURSOR_OFF | HD44780_BLINK_OFF | HD44780_DISP_ON;
    _lcd_command(dev, HD44780_DISP_CONTROL | dev->lcd_control);

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_power_on(struct hd44780_device *dev, uint8_t power_on) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    if (power_on)
        dev->lcd_control |= HD44780_DISP_ON;
    else
        dev->lcd_control &= ~HD44780_DISP_ON;

    _lcd_command(dev, HD44780_DISP_CONTROL | dev->lcd_control);

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_clear(struct hd44780_device *dev) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;
    _lcd_command(dev, HD44780_CLEAR_DISP);
    return hd44780_error_SUCCESS;
}

uint8_t hd44780_blink_on(struct hd44780_device *dev, uint8_t blink_on) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    if (blink_on)
        dev->lcd_control |= HD44780_BLINK_ON;
    else
        dev->lcd_control &= ~HD44780_BLINK_ON;

    _lcd_command(dev, HD44780_DISP_CONTROL | dev->lcd_control);

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_cursor_on(struct hd44780_device *dev, uint8_t cursor_on) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    if (cursor_on)
        dev->lcd_control |= HD44780_CURSOR_ON;
    else
        dev->lcd_control &= ~HD44780_CURSOR_ON;

    _lcd_command(dev, HD44780_DISP_CONTROL | dev->lcd_control);

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_add_char(struct hd44780_device *dev, uint8_t location, uint8_t *charmap) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    _lcd_command(dev, HD44780_SET_CGRAM_ADDR | ((location & 0x7) << 3));
    for (uint8_t i = 0; i < 8;) {
        _lcd_data(dev, charmap[i++]);
    }

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_set_cursor(struct hd44780_device *dev, uint8_t x, uint8_t y) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;
    if (x > HD44780_COLUMNS_NUM || y > HD44780_ROWS_NUM) return hd44780_error_BADPAR;

    static uint8_t y_ofset[] = {0x00, 0x40, 0x14, 0x54};

    _lcd_command(dev, HD44780_SET_DDRAM_ADDR | (x + y_ofset[y]));

    return hd44780_error_SUCCESS;
}

uint8_t hd44780_puts(struct hd44780_device *dev, char *str) {
    hd44780_error_t err;
    if ((err = _device_check(dev))) return err;

    while (*str)
        _lcd_data(dev, *str++);

    return hd44780_error_SUCCESS;
}

const char *hd44780_strerr(hd44780_error_t err) {
    switch (err) {
        case hd44780_error_SUCCESS: return "Success";
        case hd44780_error_NODEV: return "Struct hd44780_device null";
        case hd44780_error_NODRV: return "Struct hd44780_driver null";
        case hd44780_error_NODRVF: return "Struct hd44780_driver function null";
        case hd44780_error_BADPAR: return "Invalid parameter";
        default: break;
    }

    return "undefined";
}