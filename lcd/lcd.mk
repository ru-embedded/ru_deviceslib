#### hd44780 configuration ####
# Sets num of rows and collums
# C_DEFINES += HD44780_ROWS_NUM=2
# C_DEFINES += HD44780_COLUMNS_NUM=16

#### st7735s configuration ####
# Sets display dimensions
# C_DEFINES += ST7735S_HEIGHT=128
# C_DEFINES += ST7735S_WIDTH=128