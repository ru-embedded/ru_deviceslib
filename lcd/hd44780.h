/*
 *  hd44780.h
 *
 *  Created on: 2020-01-05
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_HD44780_H_
#define _RU_DEVICESLIB_HD44780_H_

#include <stdint.h>

#ifndef HD44780_ROWS_NUM
/**
 * Configure display rows num in Makefile by adding line
 * DEPS += -DHD44780_ROWS_NUM=2
 */
#define HD44780_ROWS_NUM 2
#warning "HD44780_ROWS_NUM is undefined, value of HD44780_ROWS_NUM set to 2"
#endif  // !HD44780_ROWS_NUM

#ifndef HD44780_COLUMNS_NUM
/**
 * Configure display rows num in Makefile by adding line
 * DEPS += -DHD44780_COLUMNS_NUM=16
 */
#define HD44780_COLUMNS_NUM 16
#warning "HD44780_COLUMNS_NUM is undefined, value of HD44780_COLUMNS_NUM set to 2"
#endif  // !HD44780_COLUMNS_NUM

// TODO add HD44780_DISPLAY_MODE to lcd.mk

typedef enum hd44780_error_type {
    hd44780_error_SUCCESS = 0,
    hd44780_error_NODEV,
    hd44780_error_NODRV,
    hd44780_error_NODRVF,
    hd44780_error_BADPAR,
    hd44780_error_SIZE,
} hd44780_error_t;

struct hd44780_driver {
    uint8_t (*rs_pin_set_func)(uint8_t);
    uint8_t (*rw_pin_set_func)(uint8_t);
    uint8_t (*e_pin_set_func)(uint8_t);
    uint8_t (*data_set_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct hd44780_device {
    const struct hd44780_driver *driver;
    uint8_t lcd_control;
};

/**
 * initializing led dot display
 * !! GPIO should be configured before
 */
uint8_t hd44780_init(struct hd44780_device *dev);

/**
 * Display power on/off
 */
uint8_t hd44780_power_on(struct hd44780_device *dev, uint8_t power_on);

/**
 * Display clear
 */
uint8_t hd44780_clear(struct hd44780_device *dev);

/**
 * Display blink on/off
 */
uint8_t hd44780_blink_on(struct hd44780_device *dev, uint8_t blink_on);

/**
 * Display cursor on/off
 */
uint8_t hd44780_cursor_on(struct hd44780_device *dev, uint8_t cursor_on);

// uint8_t hd44780_set_left_to_right();
// uint8_t hd44780_set_right_to_left();
// uint8_t hd44780_autoscroll_on(struct hd44780_device *dev, uint8_t autoscroll_on);

uint8_t hd44780_add_char(struct hd44780_device *dev, uint8_t location, uint8_t *charmap);

/**
 * Set cursor position
 */
uint8_t hd44780_set_cursor(struct hd44780_device *dev, uint8_t x, uint8_t y);

/**
 * Puts text to lcd
 */
uint8_t hd44780_puts(struct hd44780_device *dev, char *str);

/**
 * Return more info about occurred error
 */
const char *hd44780_strerr(hd44780_error_t err);

#endif /* !_RU_DEVICESLIB_HD44780_H_ */
