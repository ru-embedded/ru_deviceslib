/*
 *  st7735s.c
 *
 *  Created on: 2021-01-16
 *  Author: rafau
 */

#include "st7735s.h"

#include <stdio.h>
#include <stdlib.h>

#define ST7735_NOP     0x00
#define ST7735_SWRESET 0x01
#define ST7735_RDDID   0x04
#define ST7735_RDDST   0x09

#define ST7735_SLPIN  0x10
#define ST7735_SLPOUT 0x11
#define ST7735_PTLON  0x12
#define ST7735_NORON  0x13

#define ST7735_RDDPM      0x0A
#define ST7735_RDD_MADCTL 0x0B
#define ST7735_RDD_COLMOD 0x0C
#define ST7735_RDDIM      0x0D
#define ST7735_RDDSM      0x0E
#define ST7735_RDDSDR     0x0F
#define ST7735_SLPIN      0x10
#define ST7735_SLPOUT     0x11
#define ST7735_PTLON      0x12
#define ST7735_NORON      0x13

#define ST7735_INVOFF 0x20
#define ST7735_INVON  0x21

#define ST7735_GAMSET 0x26

#define ST7735_DISPOFF 0x28
#define ST7735_DISPON  0x29
#define ST7735_CASET   0x2A
#define ST7735_RASET   0x2B
#define ST7735_RAMWR   0x2C
#define ST7735_RAMRD   0x2E

#define ST7735_PTLAR 0x30

#define ST7735_SCRLAR 0x33
#define ST7735_TEOFF  0x34
#define ST7735_TEON   0x35
#define ST7735_MADCTL 0x36
#define ST7735_VSCSAD 0x37
#define ST7735_IDMOFF 0x38
#define ST7735_IDMON  0x39
#define ST7735_COLMOD 0x3A
#define ST7735_MADCTL 0x36

#define ST7735_FRMCTR1 0xB1
#define ST7735_FRMCTR2 0xB2
#define ST7735_FRMCTR3 0xB3
#define ST7735_INVCTR  0xB4

#define ST7735_PWCTR1 0xC0
#define ST7735_PWCTR2 0xC1
#define ST7735_PWCTR3 0xC2
#define ST7735_PWCTR4 0xC3
#define ST7735_PWCTR5 0xC4
#define ST7735_VMCTR1 0xC5

#define ST7735_RDID1 0xDA
#define ST7735_RDID2 0xDB
#define ST7735_RDID3 0xDC
#define ST7735_RDID4 0xDD

#define ST7735_PWCTR6 0xFC

#define ST7735_GMCTRP1 0xE0
#define ST7735_GMCTRN1 0xE1

static st7735s_error_t _device_check(struct st7735s_device *dev) {
    if (!dev) return st7735s_error_NODEV;

    if (!dev->driver) return st7735s_error_NODRV;

    if (!dev->driver->rst_pin_set_func || !dev->driver->ao_pin_set_func || !dev->driver->spi_transfer_func ||
        !dev->driver->delay_ms_func)
        return st7735s_error_NODRVF;

    return st7735s_error_SUCCESS;
}

static void _write_command(struct st7735s_device *dev, uint8_t value) {
    dev->driver->ao_pin_set_func(0);
    dev->driver->spi_transfer_func(value);
}

static void _write_data_byte(struct st7735s_device *dev, uint8_t value) {
    dev->driver->ao_pin_set_func(1);
    dev->driver->spi_transfer_func(value);
}

static void _write_data_word(struct st7735s_device *dev, uint16_t word) {
    dev->driver->ao_pin_set_func(1);
    dev->driver->spi_transfer_func((uint8_t)(word >> 8) & 0xFF);
    dev->driver->spi_transfer_func((uint8_t)word & 0xFF);
}

static void _write_data_bytes(struct st7735s_device *dev, uint8_t *data, uint16_t size) {
    dev->driver->ao_pin_set_func(1);

    for (uint16_t i = 0; i < size;) {
        dev->driver->spi_transfer_func(data[i++]);
    }
}

static void _hard_reset(struct st7735s_device *dev) {
    dev->driver->rst_pin_set_func(1);
    dev->driver->delay_ms_func(10);
    dev->driver->rst_pin_set_func(0);
    dev->driver->delay_ms_func(10);
    dev->driver->rst_pin_set_func(1);
}

static void _set_address_window(struct st7735s_device *dev, uint16_t xs, uint16_t ys, uint16_t xe, uint16_t ye) {
    _write_command(dev, ST7735_CASET);
    _write_data_word(dev, xs);
    _write_data_word(dev, xe);

    _write_command(dev, ST7735_RASET);
    _write_data_word(dev, ys);
    _write_data_word(dev, ye);
}

uint8_t st7735s_init(struct st7735s_device *dev) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t data_buffer[6];

    _hard_reset(dev);

    _write_command(dev, ST7735_SWRESET);
    dev->driver->delay_ms_func(120);

    _write_command(dev, ST7735_SLPOUT);
    dev->driver->delay_ms_func(120);

    _write_command(dev, ST7735_COLMOD);
    _write_data_byte(dev, 0x05);

    _write_command(dev, ST7735_FRMCTR1);
    data_buffer[0] = 0x01;
    data_buffer[1] = 0x2C;
    data_buffer[2] = 0x2D;
    _write_data_bytes(dev, data_buffer, 3);

    _write_command(dev, ST7735_FRMCTR2);
    data_buffer[0] = 0x01;
    data_buffer[1] = 0x2C;
    data_buffer[2] = 0x2D;
    _write_data_bytes(dev, data_buffer, 3);

    _write_command(dev, ST7735_FRMCTR3);
    data_buffer[0] = 0x01;
    data_buffer[1] = 0x2C;
    data_buffer[2] = 0x2D;
    data_buffer[3] = 0x01;
    data_buffer[4] = 0x2C;
    data_buffer[5] = 0x2D;
    _write_data_bytes(dev, data_buffer, 6);

    _write_command(dev, ST7735_INVCTR);
    _write_data_byte(dev, 0x07);

    _write_command(dev, ST7735_PWCTR1);
    data_buffer[0] = 0xA2;
    data_buffer[1] = 0x02;
    data_buffer[2] = 0x84;
    _write_data_bytes(dev, data_buffer, 3);

    _write_command(dev, ST7735_PWCTR2);
    _write_data_byte(dev, 0xC5);

    _write_command(dev, ST7735_PWCTR3);
    data_buffer[0] = 0x0A;
    data_buffer[1] = 0x00;
    _write_data_bytes(dev, data_buffer, 2);

    _write_command(dev, ST7735_PWCTR4);
    data_buffer[0] = 0x8A;
    data_buffer[1] = 0x2A;
    _write_data_bytes(dev, data_buffer, 2);

    _write_command(dev, ST7735_PWCTR5);
    data_buffer[0] = 0x8A;
    data_buffer[1] = 0xEE;
    _write_data_bytes(dev, data_buffer, 2);

    _write_command(dev, ST7735_VMCTR1);
    _write_data_byte(dev, 0x0E);

    _write_command(dev, ST7735_INVOFF);

    _write_command(dev, ST7735_MADCTL);
    _write_data_byte(dev, st7735s_orientation_PORTAIR);

    _write_command(dev, ST7735_DISPON);

    dev->inverted = 0;
    dev->orientation = st7735s_orientation_PORTAIR;

    return st7735s_error_SUCCESS;
}

uint8_t st7735s_set_pixel(struct st7735s_device *dev, uint16_t x, uint16_t y, uint16_t color) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    _set_address_window(dev, x, y, x, y);

    _write_command(dev, ST7735_RAMWR);
    _write_data_word(dev, color);

    return st7735s_error_SUCCESS;
}

uint8_t st7735s_fill_rectangle(struct st7735s_device *dev, uint16_t x, uint16_t y, uint16_t width, uint16_t height,
                               uint16_t color) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    uint16_t x1 = x + width - 1;
    uint16_t y1 = y + height - 1;

    if (x1 > ST7735S_WIDTH) x1 = ST7735S_WIDTH;
    if (y1 > ST7735S_HEIGHT) y1 = ST7735S_HEIGHT;

    _set_address_window(dev, x, y, x1, y1);

    dev->driver->ao_pin_set_func(1);

    for (uint16_t x = 0; x < x1; ++x) {
        for (uint16_t y = 0; y < y1; ++y) {
            dev->driver->spi_transfer_func((uint8_t)(color >> 8) & 0xFF);
            dev->driver->spi_transfer_func((uint8_t)color & 0xFF);
        }
    }

    return st7735s_error_SUCCESS;
}

uint8_t st7735s_fill_display(struct st7735s_device *dev, uint16_t color) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    int16_t size = (ST7735S_WIDTH + 1) * (ST7735S_HEIGHT + 2);

    _set_address_window(dev, 0, 0, ST7735S_WIDTH + 1, ST7735S_HEIGHT + 2);

    _write_command(dev, ST7735_RAMWR);

    dev->driver->ao_pin_set_func(1);

    for (uint16_t i = 0; i < size; ++i) {
        dev->driver->spi_transfer_func((uint8_t)(color >> 8) & 0xFF);
        dev->driver->spi_transfer_func((uint8_t)color & 0xFF);
    }

    return st7735s_error_SUCCESS;
}

uint8_t st7735s_set_inversion(struct st7735s_device *dev, uint8_t inversion) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    if (inversion == dev->inverted) return st7735s_error_SUCCESS;

    if (inversion)
        _write_command(dev, ST7735_INVON);
    else
        _write_command(dev, ST7735_INVOFF);

    dev->inverted = !!inversion;

    return st7735s_error_SUCCESS;
}

uint8_t st7735s_set_orientation(struct st7735s_device *dev, st7735s_orientation_t orientation) {
    st7735s_error_t err;
    if ((err = _device_check(dev))) return err;

    if (orientation != st7735s_orientation_PORTAIR && orientation != st7735s_orientation_LANDSCAPE &&
        orientation != st7735s_orientation_PORTRAIT_SWAPPED && orientation != st7735s_orientation_LANDSCAPE_SWAPPED) {
        return st7735s_error_BADPAR;
    }

    if (orientation == dev->orientation) return st7735s_error_SUCCESS;

    _write_command(dev, ST7735_MADCTL);
    _write_data_byte(dev, orientation);

    dev->orientation = orientation;

    return st7735s_error_SUCCESS;
}

const char *st7735s_strerr(st7735s_error_t err) {
    switch (err) {
        case st7735s_error_SUCCESS: return "Success";
        case st7735s_error_NODEV: return "Struct st7735s_device is null";
        case st7735s_error_NODRV: return "Struct st7735s_driver is null";
        case st7735s_error_NODRVF: return "Struct st7735s_driver function is null";
        case st7735s_error_BADPAR: return "Invalid parameter";
        default: break;
    }

    return "undefined";
}
