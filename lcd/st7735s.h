/*
 *  st7735s.h
 *
 *  Created on: 2021-01-16
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_ST7735S_H_
#define _RU_DEVICESLIB_ST7735S_H_

#include <stdint.h>

#ifndef ST7735S_HEIGHT
/**
 * Configure display rows num in Makefile by adding line
 */
#define ST7735S_HEIGHT 128
#warning "ST7735S_HEIGHT is undefined, value of ST7735S_HEIGHT set to 2"
#endif  // !ST7735S_HEIGHT

#ifndef ST7735S_WIDTH
/**
 * Configure display rows num in Makefile by adding line
 */
#define ST7735S_WIDTH 128
#warning "ST7735S_WIDTH is undefined, value of ST7735S_WIDTH set to 128"
#endif  // !ST7735S_WIDTH

typedef enum st7735s_error_type {
    st7735s_error_SUCCESS = 0,
    st7735s_error_NODEV,
    st7735s_error_NODRV,
    st7735s_error_NODRVF,
    st7735s_error_BADPAR,
    st7735s_error_SIZE,
} st7735s_error_t;

typedef enum st7735s_orientation {
    st7735s_orientation_PORTAIR = 0x00,
    st7735s_orientation_LANDSCAPE = 0x60,
    st7735s_orientation_PORTRAIT_SWAPPED = 0xC0,
    st7735s_orientation_LANDSCAPE_SWAPPED = 0xA0,
} st7735s_orientation_t;

struct st7735s_driver {
    uint8_t (*rst_pin_set_func)(uint8_t);
    uint8_t (*ao_pin_set_func)(uint8_t);
    uint8_t (*spi_transfer_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct st7735s_device {
    const struct st7735s_driver *driver;
    uint8_t inverted;
    st7735s_orientation_t orientation;
};

/**
 * initializing led ttf display
 */
uint8_t st7735s_init(struct st7735s_device *dev);

uint8_t st7735s_set_inversion(struct st7735s_device *dev, uint8_t inversion);

uint8_t st7735s_set_orientation(struct st7735s_device *dev, st7735s_orientation_t orientation);

uint8_t st7735s_set_pixel(struct st7735s_device *dev, uint16_t x, uint16_t y, uint16_t color);

uint8_t st7735s_fill_rectangle(struct st7735s_device *dev, uint16_t x, uint16_t y, uint16_t width, uint16_t height,
                               uint16_t color);

uint8_t st7735s_fill_display(struct st7735s_device *dev, uint16_t color);

/**
 * Return more info about occurred error
 */
const char *st7735s_strerr(st7735s_error_t err);

#endif /* !_RU_DEVICESLIB_ST7735S_H_ */
