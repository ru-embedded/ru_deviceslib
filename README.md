# ru_deviceslib

## How to add to project

Adding submodule to project:

```bash
mkdir -p libs && cd libs
git submodule add https://gitlab.com/ru-embedded/ru_deviceslib.git
# or
git submodule add git@gitlab.com:ru-embedded/ru_deviceslib.git
```

and add modules to your makefile:

- run script to generate `ru_deviceslib.mk` file

```bash
./libs/ru_deviceslib/create_mk.sh
```

- include `ru_deviceslib.mk` to your Makefile

```Makefile
include ./ru_deviceslib.mk
```

- add headers and sources to Makefile (example):

```Makefile
INCDIRS += ./libs/
SRC += libs/ru_deviceslib/max7219/led8x8.c
```

## libs

<!-- #### ad9850 -->

### lcd

- hd44780
- st7735s - TTF LCD SPI controlled display

### led8x8

- lex8x8 - led 8x8 display with **max7219** shift register, can handle several series connected modules

### oled

- ssd1306 - oled display with ssd1306 i2c driver

### flash

- w25qxx - spi flash memory on w25qxx chip

### gpio_expander

- mcp23017
