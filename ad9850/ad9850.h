/*
 *  ad9850.h
 *
 *  Created on: 2019-11-10
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_AD9850_H_
#define _RU_DEVICESLIB_AD9850_H_

#include <stdint.h>

typedef enum ad9850_error_type {
    ad9850_error_SUCCESS = 0,
    ad9850_error_NODEV,
    ad9850_error_NODRV,
    ad9850_error_NODRVF,
    ad9850_error_BADPAR,
    ad9850_error_SIZE,
} ad9850_error_t;

struct ad9850_driver {
    uint8_t (*w_clk_pin_set_func)(uint8_t);
    uint8_t (*fq_ud_pin_set_func)(uint8_t);
    uint8_t (*data_pin_set_func)(uint8_t);
    uint8_t (*reset_pin_set_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct ad9850_device {
    const struct ad9850_driver *driver;

    uint32_t d_phase;
    uint8_t phase;
    double c_freq;
};

/**
 * Initialize ad9850 frequency generator
 */
uint8_t ad9850_init(struct ad9850_device *dev);

/**
 * Set calibtation freq value
 */
uint8_t ad9850_calibrate(struct ad9850_device *dev, double trim_freq);

/**
 * Set freqency and phase of generating wave
 */
uint8_t ad9850_set_freq(struct ad9850_device *dev, double freq, uint8_t phase);

/**
 * Return more info about occured error
 */
const char *ad9850_strerr(ad9850_error_t err);

#endif /* !_RU_DEVICESLIB_AD9850_H_ */
