/*
 *  ad9850.c
 *
 *  Created on: 2019-11-10
 *  Author: rafau
 */

#include "ad9850.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static ad9850_error_t _device_check(struct ad9850_device *dev) {
    if (!dev) return ad9850_error_NODEV;
    if (!dev->driver) return ad9850_error_NODRV;
    if (!dev->driver->w_clk_pin_set_func || !dev->driver->fq_ud_pin_set_func || !dev->driver->data_pin_set_func ||
        !dev->driver->reset_pin_set_func || !dev->driver->delay_ms_func)
        return ad9850_error_NODRVF;
    return ad9850_error_SUCCESS;
}

static void _pulse_pin(uint8_t (*pin_set_func)(uint8_t), uint8_t (*delay_ms_func)(uint32_t)) {
    pin_set_func(1);
    delay_ms_func(10);
    pin_set_func(0);
}

static void _shift_out(struct ad9850_device *dev, uint8_t val) {
    for (uint8_t i = 0; i < 8; i++) {
        dev->driver->data_pin_set_func(!!(val & (1 << i)));
        _pulse_pin(dev->driver->w_clk_pin_set_func, dev->driver->delay_ms_func);
    }
}

static void _update(struct ad9850_device *dev) {
    for (int i = 0; i < 4; i++, dev->d_phase >>= 8) { _shift_out(dev, dev->d_phase & 0xFF); }
    _shift_out(dev, dev->phase & 0xFF);
    _pulse_pin(dev->driver->fq_ud_pin_set_func, dev->driver->delay_ms_func);
}

static void _down(struct ad9850_device *dev) {
    _pulse_pin(dev->driver->fq_ud_pin_set_func, dev->driver->delay_ms_func);
    _shift_out(dev, 0x04);
    _pulse_pin(dev->driver->fq_ud_pin_set_func, dev->driver->delay_ms_func);
}

static void _up(struct ad9850_device *dev) {
    _update(dev);
}

uint8_t ad9850_init(struct ad9850_device *dev) {
    ad9850_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->d_phase = 0;
    dev->phase = 0;
    dev->c_freq = 125000000;

    _pulse_pin(dev->driver->reset_pin_set_func, dev->driver->delay_ms_func);
    _pulse_pin(dev->driver->w_clk_pin_set_func, dev->driver->delay_ms_func);
    _pulse_pin(dev->driver->fq_ud_pin_set_func, dev->driver->delay_ms_func);

    return ad9850_error_SUCCESS;
}

uint8_t ad9850_calibrate(struct ad9850_device *dev, double trim_freq) {
    ad9850_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->c_freq = trim_freq;
    return ad9850_error_SUCCESS;
}

uint8_t ad9850_set_freq(struct ad9850_device *dev, double freq, uint8_t phase) {
    ad9850_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->d_phase = (freq * 4294967296.0) / dev->c_freq;
    dev->phase = phase << 3;
    _update(dev);

    return ad9850_error_SUCCESS;
}

const char *ad9850_strerr(ad9850_error_t err) {
    switch (err) {
        case ad9850_error_SUCCESS: return "Success";
        case ad9850_error_NODEV: return "Struct ad9850_device null";
        case ad9850_error_NODRV: return "Struct ad9850_driver null";
        case ad9850_error_NODRVF: return "Struct ad9850_driver function null";
        case ad9850_error_BADPAR: return "Invalid parameter";
        default: break;
    }

    return "undefined";
}