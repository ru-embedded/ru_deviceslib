/*
 *  ssd1306.c
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#include "ssd1306.h"

#include <stdio.h>
#include <stdlib.h>

#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF
#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA
#define SSD1306_SETVCOMDETECT 0xDB
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9
#define SSD1306_SETMULTIPLEX 0xA8
#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10
#define SSD1306_SETSTARTLINE 0x40
#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR 0x22
#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8
#define SSD1306_SEGREMAP 0xA0
#define SSD1306_CHARGEPUMP 0x8D
#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2
// Scrolling #defines
#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A

static const uint8_t _init_buffer[] = {0x80,                            // control byte
                                       SSD1306_DISPLAYOFF, 0x00,        // Low Column
                                       SSD1306_SETHIGHCOLUMN, 0xB0,     // Page
                                       SSD1306_SETSTARTLINE, 0xA1,      // remap
                                       SSD1306_SETCOMPINS, 0x12,        //
                                       SSD1306_SETDISPLAYOFFSET, 0x00,  // NO offset
                                       SSD1306_COMSCANINC, SSD1306_COMSCANDEC, SSD1306_NORMALDISPLAY,
                                       SSD1306_DISPLAYALLON_RESUME,       // display ON
                                       SSD1306_SETCONTRAST, 0x50,         // set contrast
                                       SSD1306_SETMULTIPLEX, 0x3f,        // multiplex ratio (1/64 duty)
                                       SSD1306_SETDISPLAYCLOCKDIV, 0x80,  // Display clock divide
                                       SSD1306_SETPRECHARGE, 0xF1,        // precharge period
                                       SSD1306_SETVCOMDETECT, 0x40,       //
                                       SSD1306_CHARGEPUMP, 0x14,          //
                                                                          // SSD1306_INVERTDISPLAY,//
                                       SSD1306_DISPLAYON};

static uint8_t _command(struct ssd1306 *oled, uint8_t command) {
    uint8_t buffer[2];
    buffer[0] = 0x80;
    buffer[1] = command;

    return oled->i2c_send_func(SSD1306_ADDRESS, buffer, sizeof(buffer));
}

static uint8_t _data(struct ssd1306 *oled, uint8_t data) {
    uint8_t buffer[2];
    buffer[0] = 0x40;
    buffer[1] = data;

    return oled->i2c_send_func(SSD1306_ADDRESS, buffer, sizeof(buffer));
}

static inline uint8_t _set_column(struct ssd1306 *oled, uint8_t column) {
    if (column > SSD1306_LCD_WIDTH) return ssd1306_error_BADPAR;
    if (_command(oled, SSD1306_SETLOWCOLUMN | (column & 0x0F))) return ssd1306_error_I2CERR;
    if (_command(oled, SSD1306_SETHIGHCOLUMN | (column >> 4))) return ssd1306_error_I2CERR;
    return ssd1306_error_SUCCESS;
}

static inline uint8_t _set_page(struct ssd1306 *oled, uint8_t page) {
    if (page > 7) return ssd1306_error_BADPAR;
    if (_command(oled, 0xB0 | page)) return ssd1306_error_I2CERR;
    return ssd1306_error_SUCCESS;
}

static inline uint8_t _set_pos(struct ssd1306 *oled, uint8_t column, uint8_t page) {
    uint8_t ret;

    ret = _set_column(oled, column);
    if (ret) return ret;

    ret = _set_page(oled, page);
    if (ret) return ret;

    return ssd1306_error_SUCCESS;
}

static inline uint8_t _set_line(struct ssd1306 *oled, uint8_t line) {
    if (line > 32) return ssd1306_error_BADPAR;
    if (_command(oled, SSD1306_SETSTARTLINE | line)) return ssd1306_error_I2CERR;
    return ssd1306_error_SUCCESS;
}

static inline uint8_t _write_char(struct ssd1306 *oled, uint8_t c) {
    uint16_t base = c * 6;
    uint8_t ret;
    for (uint8_t i = 0; i < 6; ++i) {
        ret = _data(oled, oled->font[base + i]);
        if (ret) return ret;
    }
    return ssd1306_error_SUCCESS;
}

uint8_t ssd1306_init(struct ssd1306 *oled) {
    if (!oled) return ssd1306_error_INSTNULL;
    if (!oled->i2c_send_func) return ssd1306_error_NOI2CFUN;

    uint8_t r = oled->i2c_send_func(SSD1306_ADDRESS, (uint8_t *)_init_buffer, sizeof(_init_buffer));

    return r == 0 ? ssd1306_error_SUCCESS : ssd1306_error_I2CERR;
}

uint8_t ssd1306_clear(struct ssd1306 *oled) {
    if (!oled) return ssd1306_error_INSTNULL;
    if (!oled->i2c_send_func) return ssd1306_error_NOI2CFUN;

    uint16_t i, j;
    _set_column(oled, 0);
    _set_line(oled, 0);

    for (j = 0; j < 8; j++) {
        _set_page(oled, j);
        _set_column(oled, 0);
        for (i = 0; i < 128; i++) { _data(oled, 0x00); }
    }

    return ssd1306_error_SUCCESS;
}

uint8_t ssd1306_puts(struct ssd1306 *oled, uint8_t x_pos, uint8_t line, char *str) {
    if (!oled) return ssd1306_error_INSTNULL;
    if (!oled->i2c_send_func) return ssd1306_error_NOI2CFUN;
    if (!oled->font) return ssd1306_error_NOFONT;
    if (!str) return ssd1306_error_BADPAR;

    uint8_t ret = _set_pos(oled, x_pos, line);
    if (ret) return ret;

    while (*str) {
        ret = _write_char(oled, *str);
        if (ret) return ret;
        ++str;
    }

    return ssd1306_error_SUCCESS;
}

const char *ssd1306_strerr(ssd1306_error_t err) {
    switch (err) {
        case ssd1306_error_SUCCESS: return "Success";
        case ssd1306_error_INSTNULL: return "Struct ssd1306 pointer is null";
        case ssd1306_error_BADPAR: return "Invalid parameter";
        case ssd1306_error_NOI2CFUN: return "I2C transfer function not set";
        case ssd1306_error_I2CERR: return "I2C transfer failed";
        case ssd1306_error_NOFONT: return "Font not set";
        default: break;
    }

    return "undefined";
}