/*
 *  ssd1306.h
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_SSD1306_H_
#define _RU_DEVICESLIB_SSD1306_H_

#include <stdint.h>

#define SSD1306_ADDRESS 0x3C
// #define SSD1306_ADDRESS 0x78

#ifdef OLED_DISPLAY_WIDTH
#define SSD1306_LCD_WIDTH OLED_DISPLAY_WIDTH
#else
#define SSD1306_LCD_WIDTH 0
#endif  // OLED_DISPLAY_WIDTH

#ifdef OLED_DISPLAY_HEIGHT
#define SSD1306_LCD_HEIGHT OLED_DISPLAY_HEIGHT
#else
#define SSD1306_LCD_HEIGHT 0
#endif  // OLED_DISPLAY_HEIGHT

/**
 * I2C send buffer fonction pointer
 * @param address - address of oled lcd
 * @param data - pointer to data buffer
 * @param size - data buffer size
 *
 * @returns 0 if successful
 */
typedef uint8_t (*ssd1306_i2c_send_func)(uint8_t address, uint8_t *data, uint8_t size);

struct ssd1306 {
    ssd1306_i2c_send_func i2c_send_func;
    const uint8_t *font;
};

typedef enum ssd1306_error_type {
    ssd1306_error_SUCCESS = 0,
    ssd1306_error_INSTNULL,
    ssd1306_error_BADPAR,
    ssd1306_error_NOI2CFUN,
    ssd1306_error_I2CERR,
    ssd1306_error_NOFONT,
    ssd1306_error_SIZE,
} ssd1306_error_t;

/**
 * Initialize oled lcd display
 */
uint8_t ssd1306_init(struct ssd1306 *oled);

/**
 * Clear oled lcd display
 */
uint8_t ssd1306_clear(struct ssd1306 *oled);

/**
 * Puts text to oled lcd on defined position
 * @param x_pos - column number
 * @param line - line number
 */
uint8_t ssd1306_puts(struct ssd1306 *oled, uint8_t x_pos, uint8_t line, char *str);

// TODO: ssd1306_printf
// TODO: ssd1306_display_bmp

/**
 * Return more info about occurred error
 */
const char *ssd1306_strerr(ssd1306_error_t err);

#endif /* !_RU_DEVICESLIB_SSD1306_H_ */
