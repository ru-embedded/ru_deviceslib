/*
 *  max7219.h
 *
 *  Created on: 2019-10-20
 *  Author: rafau
 */

#ifndef _MAX7219_H_
#define _MAX7219_H_

// MAX7219 registers
#define MAX7219_DIG0         0x01
#define MAX7219_DIG1         0x02
#define MAX7219_DIG2         0x03
#define MAX7219_DIG3         0x04
#define MAX7219_DIG4         0x05
#define MAX7219_DIG5         0x06
#define MAX7219_DIG6         0x07
#define MAX7219_DIG7         0x08
#define MAX7219_DECODE_MODE  0x09
#define MAX7219_INTENSITY    0x0A
#define MAX7219_SCAN_LIMIT   0x0B
#define MAX7219_SHUTDOWN     0x0C
#define MAX7219_DISPLAY_TEST 0x0F

// MAX7219 decode modes
#define MAX7219_DM_NO_DECODE     0x00
#define MAX7219_DM_B_CODE_DIG0   0x01
#define MAX7219_DM_B_CODE_DIG3_0 0x0F
#define MAX7219_DM_B_CODE_DIG7_0 0xFF

#endif /* !_MAX7219_H_ */