/*
 *  led8x8.h
 *
 *  Created on: 2019-10-20
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_LED8X8_H_
#define _RU_DEVICESLIB_LED8X8_H_

#include <stdint.h>

#ifndef LED8x8_DISP_NUM
/**
 * Configure displays num in Makefile by adding line
 * DEPS += -DLED8x8_DISP_NUM=4
 */
#define LED8x8_DISP_NUM 1
#warning "LED8x8_DISP_NUM is undefined, value of LED8x8_DISP_NUM set to 1"
#endif  // !LED8x8_DISP_NUM

#ifndef LED8x8_EXTRA_DISP_NUM
/**
 * Configure buffered displays num in Makefile by adding line
 * DEPS += -DLED8x8_EXTRA_DISP_NUM=0
 */
#define LED8x8_EXTRA_DISP_NUM 0
#warning "LED8x8_EXTRA_DISP_NUM is undefined, value of LED8x8_EXTRA_DISP_NUM set to 0"
#endif  // !LED8x8_EXTRA_DISP_NUM

#if LED8x8_DISP_NUM > 16
#error "Driver can handle only up to 16 displays"
#endif

#define LED8x8_ALL_DISP_NUM (LED8x8_DISP_NUM + LED8x8_EXTRA_DISP_NUM)

/**
 * pointer to output pin set/reset function
 */
typedef uint8_t (*led8x8_pin_set_func)(uint8_t);
typedef void (*led8x8_delay_ms_func)(uint32_t);

typedef union {
    uint64_t all;
    uint8_t row[8];
} led8x8_state_t;

typedef enum led8x8_error_type {
    led8x8_error_SUCCESS = 0,
    led8x8_error_NODEV,
    led8x8_error_NODRV,
    led8x8_error_NODRVF,
    led8x8_error_BADPAR,
    led8x8_error_NOFONT,
    led8x8_error_SIZE,
} led8x8_error_t;

struct led8x8_driver {
    uint8_t (*din_pin_set_func)(uint8_t);
    uint8_t (*cs_pin_set_func)(uint8_t);
    uint8_t (*clk_pin_set_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct led8x8_device {
    const struct led8x8_driver *driver;

    led8x8_state_t state[LED8x8_ALL_DISP_NUM];
    uint8_t intensity;
    uint8_t rotation;
    uint64_t *font;
};

/**
 * initializing led dot display
 * !! GPIO should be configured before
 */
uint8_t led8x8_init(struct led8x8_device *dev);

/**
 * Set led intensity
 */
uint8_t led8x8_set_intensity(struct led8x8_device *dev, uint8_t intensity);

/**
 * Set dispaly rotation
 */
uint8_t led8x8_set_rotation(struct led8x8_device *dev, uint8_t rotation);

/**
 * Write display state to shift registers
 */
uint8_t led8x8_display(struct led8x8_device *dev);

/**
 * Clears display state
 */
uint8_t led8x8_clear(struct led8x8_device *dev);

/**
 * Set selected state to specified pixel
 */
uint8_t led8x8_set_pixel(struct led8x8_device *dev, uint16_t x, uint16_t y, uint8_t state);

/**
 * Set whole 8x8 display
 */
uint8_t led8x8_set_display(struct led8x8_device *dev, uint8_t disp_num, uint64_t data);

/**
 * Shift display state n columns to right (n > 0) or left (n < 0)
 */
uint8_t led8x8_shift(struct led8x8_device *dev, int16_t n);

/**
 * Set string string to display
 */
uint8_t led8x8_puts(struct led8x8_device *dev, char *str);

/**
 * Return more info about occured error
 */
const char *led8x8_strerr(led8x8_error_t err);

#endif /* !_RU_DEVICESLIB_LED8X8_H_ */
