#### led8x8 configuration #### 

# Sets num of phisycal displays
# C_DEFINES += LED8x8_DISP_NUM=4

# Sets num of buffered displays
# C_DEFINES += LED8x8_EXTRA_DISP_NUM=3

# Prevents loading font from libs/ru_deviceslib/max7219/led8x8_font.h
# C_DEFINES += LED8x8_FONT_LOAD
