/*
 *  led8x8.c
 *
 *  Created on: 2019-10-20
 *  Author: rafau
 */

#include "led8x8.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "led8x8_font.h"
#include "max7219.h"

static led8x8_error_t _device_check(struct led8x8_device *dev) {
    if (!dev) return led8x8_error_NODEV;
    if (!dev->driver) return led8x8_error_NODRV;
    if (!dev->driver->din_pin_set_func || !dev->driver->cs_pin_set_func || !dev->driver->clk_pin_set_func ||
        !dev->driver->delay_ms_func)
        return led8x8_error_NODRVF;
    return led8x8_error_SUCCESS;
}

static void _send_16bits(struct led8x8_device *dev, uint16_t b) {
    for (uint8_t i = 0; i < 16; ++i) {
        dev->driver->clk_pin_set_func(0);
        dev->driver->din_pin_set_func(!!(b & 0x8000));
        b <<= 1;
        // dev->delay_ms_func(1);
        dev->driver->clk_pin_set_func(1);
        // dev->delay_ms_func(1);
    }
}

static inline void _write_one_register(struct led8x8_device *dev, uint8_t reg, uint8_t data) {
    _send_16bits(dev, data | (reg << 8));
}

static void _write_register(struct led8x8_device *dev, uint8_t reg, uint8_t data) {
    dev->driver->cs_pin_set_func(0);
    for (uint8_t i = 0; i < LED8x8_DISP_NUM; ++i) { _send_16bits(dev, data | (reg << 8)); }
    dev->driver->cs_pin_set_func(1);
}

static inline uint8_t _get_column(led8x8_state_t *s, uint8_t col) {
    uint8_t disp_num = col / 8;
    uint8_t disp_col = col % 8;

    // led8x8_state_t *disp = &s[LED8x8_ALL_DISP_NUM - disp_num - 1];
    led8x8_state_t *disp = &s[disp_num];
    // led8x8_state_t *disp = s + (disp_num * sizeof(led8x8_state_t));

    uint8_t value = 0x00;

    for (uint8_t i = 0; i < 8; ++i)
        if ((disp->row[i] & (1 << disp_col))) value |= (1 << i);

    return value;
}

static inline void _set_column(led8x8_state_t *s, uint8_t col, uint8_t value) {
    uint8_t disp_num = col / 8;
    uint8_t disp_col = col % 8;
    // led8x8_state_t *disp = &s[LED8x8_ALL_DISP_NUM - disp_num - 1];
    led8x8_state_t *disp = &s[disp_num];

    for (uint8_t i = 0; i < 8; ++i) {
        if (value & (1 << i))
            disp->row[i] |= (1 << disp_col);
        else
            disp->row[i] &= ~(1 << disp_col);
    }
}

uint8_t led8x8_init(struct led8x8_device *dev) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->driver->cs_pin_set_func(1);
    dev->driver->clk_pin_set_func(0);
    dev->driver->din_pin_set_func(0);

    if (dev->intensity > 0x0F) dev->intensity = 0x01;
    if (dev->rotation > 0x03) dev->rotation = 0x00;

    _write_register(dev, MAX7219_DECODE_MODE, MAX7219_DM_NO_DECODE);
    _write_register(dev, MAX7219_SCAN_LIMIT, 0x07);
    _write_register(dev, MAX7219_INTENSITY, dev->intensity);
    _write_register(dev, MAX7219_DISPLAY_TEST, 0x00);
    _write_register(dev, MAX7219_SHUTDOWN, 0x01);

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_set_intensity(struct led8x8_device *dev, uint8_t intensity) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (intensity > 0x0F) return led8x8_error_BADPAR;

    dev->intensity = intensity;

    _write_register(dev, MAX7219_INTENSITY, intensity);

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_set_rotation(struct led8x8_device *dev, uint8_t rotation) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (rotation > 0x03) return led8x8_error_BADPAR;
    dev->rotation = rotation;
    return led8x8_error_SUCCESS;
}

uint8_t led8x8_display(struct led8x8_device *dev) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t i, j;
    for (j = 0; j < 8; ++j) {
        dev->driver->cs_pin_set_func(0);
        for (i = 1; i <= LED8x8_DISP_NUM; ++i) {
            _write_one_register(dev, MAX7219_DIG0 + j, dev->state[LED8x8_DISP_NUM - i].row[j]);
        }
        dev->driver->cs_pin_set_func(1);
    }

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_clear(struct led8x8_device *dev) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;

    for (uint8_t i = 0; i < LED8x8_ALL_DISP_NUM; ++i) dev->state[i].all = 0x00;

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_set_pixel(struct led8x8_device *dev, uint16_t x, uint16_t y, uint8_t state) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (y > 8 || x > (8 * LED8x8_ALL_DISP_NUM)) return led8x8_error_BADPAR;

    uint8_t disp_x = x % 8;
    uint8_t disp_num = x >> 3;
    if (disp_num > LED8x8_ALL_DISP_NUM) return led8x8_error_BADPAR;

    if (state)
        dev->state[disp_num].row[y] |= (1 << disp_x);
    else
        dev->state[disp_num].row[y] &= ~(1 << disp_x);

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_set_display(struct led8x8_device *dev, uint8_t disp_num, uint64_t data) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (disp_num >= LED8x8_ALL_DISP_NUM) return led8x8_error_BADPAR;

    dev->state[disp_num].all = data;

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_shift(struct led8x8_device *dev, int16_t n) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (n == 0) return led8x8_error_SUCCESS;  // success with no effort ;)

    const uint8_t max_col_num = LED8x8_ALL_DISP_NUM * 8;
    int16_t dest;
    uint8_t column;

    static led8x8_state_t state[LED8x8_ALL_DISP_NUM];
    memcpy(state, dev->state, sizeof(led8x8_state_t) * LED8x8_ALL_DISP_NUM);

    if (n < 0) n = max_col_num + n;

    for (uint8_t i = 0; i < max_col_num; ++i) {
        dest = ((i + n) % max_col_num);

        column = _get_column(state, i);
        _set_column(dev->state, dest, column);
    }

    return led8x8_error_SUCCESS;
}

uint8_t led8x8_puts(struct led8x8_device *dev, char *str) {
    led8x8_error_t err;
    if ((err = _device_check(dev))) return err;
    if (!str) return led8x8_error_BADPAR;
    if (!dev->font) return led8x8_error_NOFONT;

    for (uint8_t i = 0; i < LED8x8_ALL_DISP_NUM && str[i] != '0'; ++i)
        led8x8_set_display(dev, i, led8x8_font[str[i] - ' ']);

    return led8x8_error_SUCCESS;
}

const char *led8x8_strerr(led8x8_error_t err) {
    switch (err) {
        case led8x8_error_SUCCESS: return "Success";
        case led8x8_error_NODEV: return "Struct led8x8_device null";
        case led8x8_error_NODRV: return "Struct led8x8_driver null";
        case led8x8_error_NODRVF: return "Struct led8x8_driver function null";
        case led8x8_error_BADPAR: return "Invalid parameter";
        case led8x8_error_NOFONT: return "Font not set";
        default: break;
    }

    return "undefined";
}