/*
 *  mcp23017.c
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#include "mcp23017.h"

#include <stdio.h>
#include <stdlib.h>

#define MCP23017_ADDRESS 0x20

#define MCP23017_IODIRA   0x00  // I/O direction register A
#define MCP23017_IPOLA    0x02  // Input polarity port register A
#define MCP23017_GPINTENA 0x04  // Interrupt-on-change pins A
#define MCP23017_DEFVALA  0x06  // Default value register A
#define MCP23017_INTCONA  0x08  // Interrupt-on-change control register A
#define MCP23017_IOCONA   0x0A  // I/O expander configuration register A
#define MCP23017_GPPUA    0x0C  // GPIO pull-up resistor register A
#define MCP23017_INTFA    0x0E  // Interrupt flag register A
#define MCP23017_INTCAPA  0x10  // Interrupt captured value for port register A
#define MCP23017_GPIOA    0x12  // General purpose I/O port register A
#define MCP23017_OLATA    0x14  // Output latch register 0 A

#define MCP23017_IODIRB   0x01  // I/O direction register B
#define MCP23017_IPOLB    0x03  // Input polarity port register B
#define MCP23017_GPINTENB 0x05  // Interrupt-on-change pins B
#define MCP23017_DEFVALB  0x07  // Default value register B
#define MCP23017_INTCONB  0x09  // Interrupt-on-change control register B
#define MCP23017_IOCONB   0x0B  // I/O expander configuration register B
#define MCP23017_GPPUB    0x0D  // GPIO pull-up resistor register B
#define MCP23017_INTFB    0x0F  // Interrupt flag register B
#define MCP23017_INTCAPB  0x11  // Interrupt captured value for port register B
#define MCP23017_GPIOB    0x13  // General purpose I/O port register B
#define MCP23017_OLATB    0x15  // Output latch register 0 B

#define MCP23017_INT_ERR 255  // Interrupt error

static mcp23017_error_t _device_check(struct mcp23017_device *dev) {
    if (!dev) return mcp23017_error_NODEV;

    if (!dev->driver) return mcp23017_error_NODRV;

    if (!dev->driver->i2c_write_data || !dev->driver->delay_ms_func) return mcp23017_error_NODRVF;

    return mcp23017_error_SUCCESS;
}

static uint8_t _write_register(struct mcp23017_device *dev, uint8_t reg, uint8_t value) {
    uint8_t buf[2];

    buf[0] = reg;
    buf[1] = value;

    uint8_t rc = dev->driver->i2c_write_data(MCP23017_ADDRESS | dev->addr_cfg, buf, 2);

    return rc == 0 ? mcp23017_error_SUCCESS : mcp23017_error_I2CERR;
}

uint8_t mcp23017_init(struct mcp23017_device *dev, uint8_t addr_cfg) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    if (dev->driver->rst_pin_set_func) dev->driver->rst_pin_set_func(1);
    if (addr_cfg > 0x03) return mcp23017_error_BADPAR;

    dev->addr_cfg = addr_cfg & 0x03;

    return mcp23017_error_SUCCESS;
}

uint8_t mcp23017_hard_reset(struct mcp23017_device *dev) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    if (!dev->driver->rst_pin_set_func) return mcp23017_error_NODRVF;

    dev->driver->rst_pin_set_func(0);
    dev->driver->delay_ms_func(100);
    dev->driver->rst_pin_set_func(1);

    return mcp23017_error_SUCCESS;
}

uint8_t mcp23017_set_direction(struct mcp23017_device *dev, mcp23017_port_t port, uint8_t pin_mask) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    if (port != mcp23017_port_A && port != mcp23017_port_B) return mcp23017_error_BADPAR;

    uint8_t reg = MCP23017_IODIRA | port;
    uint8_t rc = _write_register(dev, reg, pin_mask);

    return rc == 0 ? mcp23017_error_SUCCESS : mcp23017_error_I2CERR;
}

uint8_t mcp23017_set_direction_for_all(struct mcp23017_device *dev, uint16_t pin_mask) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t rc;

    rc = _write_register(dev, MCP23017_IODIRA, (uint8_t)pin_mask);
    if (rc) return mcp23017_error_I2CERR;

    rc = _write_register(dev, MCP23017_IODIRB, (uint8_t)(pin_mask >> 8));
    if (rc) return mcp23017_error_I2CERR;

    return mcp23017_error_SUCCESS;
}

uint8_t mcp23017_set_value(struct mcp23017_device *dev, mcp23017_port_t port, uint8_t pin_mask) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    if (port != mcp23017_port_A && port != mcp23017_port_B) return mcp23017_error_BADPAR;

    uint8_t reg = MCP23017_OLATA | port;
    uint8_t rc = _write_register(dev, reg, pin_mask);

    return rc == 0 ? mcp23017_error_SUCCESS : mcp23017_error_I2CERR;
}

uint8_t mcp23017_set_value_for_all(struct mcp23017_device *dev, uint16_t pin_mask) {
    mcp23017_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t rc;

    rc = _write_register(dev, MCP23017_OLATA, (uint8_t)pin_mask);
    if (rc) return mcp23017_error_I2CERR;

    rc = _write_register(dev, MCP23017_OLATB, (uint8_t)(pin_mask >> 8));
    if (rc) return mcp23017_error_I2CERR;

    return mcp23017_error_SUCCESS;
}

const char *mcp23017_strerr(mcp23017_error_t err) {
    switch (err) {
        case mcp23017_error_SUCCESS: return "Success";
        case mcp23017_error_NODEV: return "Struct mcp23017_device is null";
        case mcp23017_error_NODRV: return "Struct mcp23017_driver is null";
        case mcp23017_error_NODRVF: return "Struct mcp23017_driver function is null";
        case mcp23017_error_BADPAR: return "Invalid parameter";
        case mcp23017_error_I2CERR: return "I2C transfer failed";
        default: break;
    }

    return "undefined";
}