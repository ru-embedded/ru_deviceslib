/*
 *  mcp23017.h
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#ifndef __RU_DEVICESLIB_GPIO_EXPANDER_MCP23017_H__
#define __RU_DEVICESLIB_GPIO_EXPANDER_MCP23017_H__

#include <stdint.h>

typedef enum mcp23017_port_type {
    mcp23017_port_A = 0x00,
    mcp23017_port_B = 0x01,
} mcp23017_port_t;

typedef enum mcp23017_direction_type {
    mcp23017_direction_OUTPUT = 0x00,
    mcp23017_direction_INPUT = 0x01,
} mcp23017_direction_t;

typedef enum mcp23017_error_type {
    mcp23017_error_SUCCESS = 0,
    mcp23017_error_NODEV,
    mcp23017_error_NODRV,
    mcp23017_error_NODRVF,
    mcp23017_error_BADPAR,
    mcp23017_error_I2CERR,
    mcp23017_error_SIZE,
} mcp23017_error_t;

struct mcp23017_driver {
    uint8_t (*rst_pin_set_func)(uint8_t);
    uint8_t (*i2c_write_data)(uint8_t address, uint8_t *data, uint8_t size);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct mcp23017_device {
    const struct mcp23017_driver *driver;
    uint8_t addr_cfg;
};

/**
 * initializing gpio expander
 *
 * @param addr_cfg: A2 A1 A0 jumpers state
 */
uint8_t mcp23017_init(struct mcp23017_device *dev, uint8_t addr_cfg);

/**
 * Performs hard reset of gpio expander
 *
 * @note rst_pin_set_func must be set;
 */
uint8_t mcp23017_hard_reset(struct mcp23017_device *dev);

/**
 * Configure gpio pins
 */
uint8_t mcp23017_set_direction(struct mcp23017_device *dev, mcp23017_port_t port, uint8_t pin_mask);

/**
 * Configure gpio pins for all pins
 */
uint8_t mcp23017_set_direction_for_all(struct mcp23017_device *dev, uint16_t pin_mask);

/**
 * Set output gpio value
 */
uint8_t mcp23017_set_value(struct mcp23017_device *dev, mcp23017_port_t port, uint8_t pin_mask);

/**
 * Set output gpio value for all pins
 */
uint8_t mcp23017_set_value_for_all(struct mcp23017_device *dev, uint16_t pin_mask);

/**
 * Return more info about occurred error
 */
const char *mcp23017_strerr(mcp23017_error_t err);

#endif /* !__RU_DEVICESLIB_GPIO_EXPANDER_MCP23017_H__ */
