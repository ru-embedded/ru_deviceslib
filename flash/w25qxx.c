/*
 *  w25qxx.c
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#include "w25qxx.h"

static w25qxx_error_t _device_check(struct w25qxx_device *dev) {
    if (!dev) return w25qxx_error_NODEV;
    if (!dev->driver) return w25qxx_error_NODRV;
    if (!dev->driver->cs_pin_set_func || !dev->driver->spi_transfer_func || !dev->driver->delay_ms_func)
        return w25qxx_error_NODRVF;
    return w25qxx_error_SUCCESS;
}

// uint32_t _read_id(struct w25qxx_device *dev) {
//     uint8_t tmp[3];

//     flash->cs_pin_set_func(0);
//     flash->spi_transfer_func(0x9F);
//     tmp[0] = flash->spi_transfer_func(W25QXX_DUMMY_BYTE);
//     tmp[1] = flash->spi_transfer_func(W25QXX_DUMMY_BYTE);
//     tmp[2] = flash->spi_transfer_func(W25QXX_DUMMY_BYTE);
//     flash->cs_pin_set_func(1);

//     return ((tmp[0] << 16) | (tmp[1] << 8) | tmp[2]);
// }

uint8_t w25qxx_init(struct w25qxx_device *dev) {
    w25qxx_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->driver->cs_pin_set_func(1);

    // uint32_t id = _read_id();
    // TODO handle all type of flash memory
    // only w25q64fv is handled now (block count = 128)

    return w25qxx_error_SUCCESS;
}

const char *w25qxx_strerr(w25qxx_error_t err) {
    switch (err) {
        case w25qxx_error_SUCCESS: return "Success";
        case w25qxx_error_NODEV: return "Struct w25qxx_device null";
        case w25qxx_error_NODRV: return "Struct w25qxx_driver null";
        case w25qxx_error_NODRVF: return "Struct w25qxx_driver function null";
        case w25qxx_error_BADPAR: return "Invalid parameter";
        default: break;
    }

    return "undefined";
}
