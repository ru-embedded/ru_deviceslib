/*
 *  w25qxx.h
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_W25QXX_H_
#define _RU_DEVICESLIB_W25QXX_H_

#include <stdint.h>

#define W25QXX_DUMMY_BYTE 0xA5

struct w25qxx_driver {
    uint8_t (*cs_pin_set_func)(uint8_t);
    uint8_t (*spi_transfer_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct w25qxx_device {
    const struct w25qxx_driver *driver;
};

typedef enum w25qxx_error_type {
    w25qxx_error_SUCCESS = 0,
    w25qxx_error_NODEV,
    w25qxx_error_NODRV,
    w25qxx_error_NODRVF,
    w25qxx_error_BADPAR,
    w25qxx_error_SIZE,
} w25qxx_error_t;

uint8_t w25qxx_init(struct w25qxx_device *dev);

// TODO: w25qxx_xxx functionality

/**
 * Return more info about occured error
 */
const char *w25qxx_strerr(w25qxx_error_t err);

#endif /* !_RU_DEVICESLIB_W25QXX_H_ */
