#!/usr/bin/env bash
#  create_mk.sh
#  Created on: 2020-01-04
#  Author: rafau
#

readonly SELF_RELATIVE_PATH=./libs/ru_deviceslib/create_mk.sh
readonly OUTFILE=./ru_deviceslib.mk

help() {
    echo "Check yor ./libs path and run script in your project dir"
    echo -e "\t$SELF_RELATIVE_PATH"
    exit 0
}

device_conf() {
    local path=$1
    [[ -z $path ]] && return

    local full_name=${path##*/}
    local name=${full_name%.mk}

    echo -e "\n\n################ $name ################" >>$OUTFILE
    cat "$path" >>$OUTFILE
}

main() {
    [ ! -f $SELF_RELATIVE_PATH ] && help

    rm -fv $OUTFILE

    dev_mk_files=$(find ./libs/ru_deviceslib -name '*.mk')

    for dev_mk_file in $dev_mk_files; do
        device_conf "$dev_mk_file"
    done

    echo -e "Add to your Makefile\n\tinclude $OUTFILE\n"
}

main
