/*
 *  mfrc522.h
 *
 *  Created on: 2020-02-09
 *  Author: rafau
 */

#ifndef _RU_DEVICESLIB_MFRC522_H_
#define _RU_DEVICESLIB_MFRC522_H_

#include <stdint.h>

typedef enum mfrc522_error_type {
    mfrc522_error_SUCCESS = 0,
    mfrc522_error_NODEV,
    mfrc522_error_NODRV,
    mfrc522_error_NODRVF,
    mfrc522_error_BADPAR,
    mfrc522_error_SIZE,
} mfrc522_error_t;

struct mfrc522_driver {
    uint8_t (*reset_pin_set_func)(uint8_t);
    uint8_t (*cs_pin_set_func)(uint8_t);
    uint8_t (*spi_transfer_func)(uint8_t);
    uint8_t (*delay_ms_func)(uint32_t);
};

struct mfrc522_device {
    const struct mfrc522_driver* driver;
};

typedef enum mfrc522_antenna_gain {
    mfrc522_antenna_gain_18bB_1 = 0x00,
    mfrc522_antenna_gain_23bB_1 = 0x01,
    mfrc522_antenna_gain_18bB_2 = 0x03,
    mfrc522_antenna_gain_23bB_2 = 0x04,
    mfrc522_antenna_gain_33bB = 0x05,
    mfrc522_antenna_gain_38bB = 0x06,
    mfrc522_antenna_gain_43bB = 0x07,
    mfrc522_antenna_gain_48bB = 0x08,
} mfrc522_antenna_gain_t;

uint8_t mfrc522_init(struct mfrc522_device* dev);

uint8_t mfrc522_get_version(struct mfrc522_device* dev, char** version);

uint8_t mfrc522_get_antenna_gain(struct mfrc522_device* dev, mfrc522_antenna_gain_t* gain);

uint8_t mfrc522_set_antenna_gain(struct mfrc522_device* dev, mfrc522_antenna_gain_t gain);

const char* mfrc522_antenna_gain_str(mfrc522_antenna_gain_t gain);

uint8_t mfrc522_is_card_pressent(struct mfrc522_device* dev);

/**
 * Return more info about occurred error
 */
const char* mfrc522_strerr(mfrc522_error_t err);

#endif /* !_RU_DEVICESLIB_MFRC522_H_ */
