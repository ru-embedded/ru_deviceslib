/*
 *  mfrc522.c
 *
 *  Created on: 2020-02-09
 *  Author: rafau
 */

#include "mfrc522.h"

#include <stdio.h>
#include <stdlib.h>

enum mfrc522_register {
    // Command and status
    _Reserved_00h = 0x00,  // reserved for future use
    CommandReg = 0x01,     // starts and stops command execution
    ComIEnReg = 0x02,      // enable and disable interrupt request control bits
    DivIEnReg = 0x03,      // enable and disable interrupt request control bits
    ComIrqReg = 0x04,      // interrupt request bits
    DivIrqReg = 0x05,      // interrupt request bits
    ErrorReg = 0x06,       // error bits showing the error status of the last command executed
    Status1Reg = 0x07,     // communication status bits
    Status2Reg = 0x08,     // receiver and transmitter status bits
    FIFODataReg = 0x09,    // input and output of 64 byte FIFO buffer
    FIFOLevelReg = 0x0A,   // number of bytes stored in the FIFO buffer
    WaterLevelReg = 0x0B,  // level for FIFO underflow and overflow warning
    ControlReg = 0x0C,     // miscellaneous control registers
    BitFramingReg = 0x0D,  // adjustments for bit-oriented frames
    CollReg = 0x0E,        // bit position of the first bit-collision detected on the RF interface
    _Reserved_0Fh = 0x0F,  // reserved for future use

    // Command
    _Reserved_10h = 0x10,   // reserved for future use
    ModeReg = 0x11,         // defines general modes for transmitting and receiving
    TxModeReg = 0x12,       // defines transmission data rate and framing
    RxModeReg = 0x13,       // defines reception data rate and framing
    TxControlReg = 0x14,    // controls the logical behavior of the antenna driver pins TX1 and TX2
    TxASKReg = 0x15,        // controls the setting of the transmission modulation
    TxSelReg = 0x16,        // selects the internal sources for the antenna driver
    RxSelReg = 0x17,        // selects internal receiver settings
    RxThresholdReg = 0x18,  // selects thresholds for the bit decoder
    DemodReg = 0x19,        // defines demodulator settings
    _Reserved_1Ah = 0x1A,   // reserved for future use
    _Reserved_1Bh = 0x1B,   // reserved for future use
    MfTxReg = 0x1C,         // controls some MIFARE communication transmit parameters
    MfRxReg = 0x1D,         // controls some MIFARE communication receive parameters
    _Reserved_1Eh = 0x1E,   // reserved for future use
    SerialSpeedReg = 0x1F,  // selects the speed of the serial UART interface

    // Configuration
    _Reserved_20h = 0x20,  // reserved for future use
    CRCResultRegH = 0x21,  // shows the MSB and LSB values of the CRC calculation
    CRCResultRegL = 0x22,
    _Reserved_23h = 0x23,  // reserved for future use
    ModWidthReg = 0x24,    // controls the ModWidth setting?
    _Reserved_25h = 0x25,  // reserved for future use
    RFCfgReg = 0x26,       // configures the receiver gain
    GsNReg = 0x27,         // selects the conductance of the antenna driver pins TX1 and TX2 for modulation
    CWGsPReg = 0x28,       // defines the conductance of the p-driver output during periods of no modulation
    ModGsPReg = 0x29,      // defines the conductance of the p-driver output during periods of modulation
    TModeReg = 0x2A,       // defines settings for the internal timer
    TPrescalerReg = 0x2B,  // the lower 8 bits of the TPrescaler value. The 4 high bits are in TModeReg.
    TReloadRegH = 0x2C,    // defines the 16-bit timer reload value
    TReloadRegL = 0x2D,
    TCounterValueRegH = 0x2E,  // shows the 16-bit timer value
    TCounterValueRegL = 0x2F,

    // Test Registers
    _Reserved_30h = 0x30,    // reserved for future use
    TestSel1Reg = 0x31,      // general test signal configuration
    TestSel2Reg = 0x32,      // general test signal configuration
    TestPinEnReg = 0x33,     // enables pin output driver on pins D1 to D7
    TestPinValueReg = 0x34,  // defines the values for D1 to D7 when it is used as an I/O bus
    TestBusReg = 0x35,       // shows the status of the internal test bus
    AutoTestReg = 0x36,      // controls the digital self-test
    VersionReg = 0x37,       // shows the software version
    AnalogTestReg = 0x38,    // controls the pins AUX1 and AUX2
    TestDAC1Reg = 0x39,      // defines the test value for TestDAC1
    TestDAC2Reg = 0x3A,      // defines the test value for TestDAC2
    TestADCReg = 0x3B,       // shows the value of ADC I and Q channels
    _Reserved_3Ch = 0x3C,    // reserved for production tests
    _Reserved_3Dh = 0x3D,    // reserved for production tests
    _Reserved_3Eh = 0x3E,    // reserved for production tests
    _Reserved_3Fh = 0x3F,    // reserved for production tests
};

enum mfrc522_command {
    cmd_Idle = 0x00,              // no action, cancels current command execution
    cmd_Mem = 0x01,               // stores 25 bytes into the internal buffer
    cmd_GenerateRandomID = 0x02,  // generates a 10-byte random ID number
    cmd_CalcCRC = 0x03,           // activates the CRC coprocessor or performs a self-test
    cmd_Transmit = 0x04,          // transmits data from the FIFO buffer
    cmd_NoCmdChange = 0x07,  // no command change, can be used to modify the CommandReg register bits without affecting
                             // the command, for example, the PowerDown bit
    cmd_Receive = 0x08,      // activates the receiver circuits
    cmd_Transceive = 0x0C,   // transmits data from FIFO buffer to antenna
                             // and automatically activates the receiver after transmission
    cmd_MFAuthent = 0x0E,    // performs the MIFARE standard authentication as a reader
    cmd_SoftReset = 0x0F     // resets the MFRC522
};

static mfrc522_error_t _device_check(struct mfrc522_device* dev) {
    if (!dev) return mfrc522_error_NODEV;
    if (!dev->driver) return mfrc522_error_NODRV;
    if (!dev->driver->reset_pin_set_func || !dev->driver->cs_pin_set_func || !dev->driver->spi_transfer_func ||
        !dev->driver->delay_ms_func)
        return mfrc522_error_NODRVF;
    return mfrc522_error_SUCCESS;
}

static void _write_register(struct mfrc522_device* dev, uint8_t address, uint8_t value) {
    uint8_t addr = (address << 1);
    dev->driver->cs_pin_set_func(0);
    dev->driver->spi_transfer_func(addr);
    dev->driver->spi_transfer_func(value);
    dev->driver->cs_pin_set_func(1);
}

static void _write_registers(struct mfrc522_device* dev, uint8_t address, uint8_t size, uint8_t* data) {
    uint8_t addr = (address << 1);

    dev->driver->cs_pin_set_func(0);

    dev->driver->spi_transfer_func(addr);
    for (uint8_t i = 0; i < size;) { dev->driver->spi_transfer_func(data[i++]); }

    dev->driver->cs_pin_set_func(1);
}

static uint8_t _read_register(struct mfrc522_device* dev, uint8_t address) {
    uint8_t addr = 0x80 | (address << 1);
    dev->driver->cs_pin_set_func(0);
    dev->driver->spi_transfer_func(addr);
    uint8_t value = dev->driver->spi_transfer_func(0x00);
    dev->driver->cs_pin_set_func(1);
    return value;
}

static void _read_registers(struct mfrc522_device* dev, uint8_t address, uint8_t size, uint8_t* data) {
    uint8_t i;
    dev->driver->cs_pin_set_func(0);

    uint8_t addr = 0x80 | (address << 1);
    dev->driver->spi_transfer_func(addr);

    for (i = 0; i < size; ++i) {
        addr = 0x80 | ((address + i) << 1);
        data[i] = dev->driver->spi_transfer_func(addr);
    }

    data[i] = dev->driver->spi_transfer_func(0x00);

    dev->driver->cs_pin_set_func(1);
}

static uint8_t _soft_reset(struct mfrc522_device* dev) {
    _write_register(dev, CommandReg, cmd_SoftReset);
    dev->driver->delay_ms_func(50);

    // Wait for the PowerDown bit in CommandReg to be cleared
    while (_read_register(dev, CommandReg) & (1 << 4)) {}

    return 0;
}

static void _antenna_power(struct mfrc522_device* dev, uint8_t power_on) {
    uint8_t value = _read_register(dev, TxControlReg);
    if (power_on && ((value & 0x03) != 0x03)) {
        _write_register(dev, TxControlReg, value | 0x03);
    } else {
        _write_register(dev, TxControlReg, value & (~0x03));
    }
}

static uint8_t _to_card(uint8_t command, uint8_t* data_tx, uint8_t size_tx, uint8_t* data_rx, uint8_t size_rx) {
    // TODO: communication with rfid card
}

uint8_t mfrc522_init(struct mfrc522_device* dev) {
    mfrc522_error_t err;
    if ((err = _device_check(dev))) return err;

    dev->driver->cs_pin_set_func(1);
    dev->driver->reset_pin_set_func(1);

    dev->driver->delay_ms_func(50);
    _soft_reset(dev);

    // Reset baud rates
    _write_register(dev, TxModeReg, 0x00);
    _write_register(dev, RxModeReg, 0x00);

    // Reset ModWidthReg
    _write_register(dev, ModWidthReg, 0x26);

    _write_register(dev, TModeReg, 0x80);  // TAuto=1; timer starts automatically at the end of the transmission in all
                                           // communication modes at all speeds
    _write_register(dev, TPrescalerReg, 0xA9);  // TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 =>
                                                // f_timer=40kHz, ie a timer period of 25μs.
    _write_register(dev, TReloadRegH, 0x03);    // Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
    _write_register(dev, TReloadRegL, 0xE8);

    _write_register(dev, TxASKReg, 0x40);  // Default 0x00.
                                           // Force a 100 % ASK modulation independent of the ModGsPReg register setting
    _write_register(dev, ModeReg, 0x3D);   // Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC
                                           // command to 0x6363 (ISO 14443-3 part 6.2.4)
    _antenna_power(dev, 1);                // Enable the antenna driver pins TX1 and TX2

    return mfrc522_error_SUCCESS;
}

uint8_t mfrc522_get_version(struct mfrc522_device* dev, char** version) {
    static char* _v1 = "MFRC522 version 1.0";
    static char* _v2 = "MFRC522 version 2.0";

    mfrc522_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t ver = _read_register(dev, VersionReg);

    if (ver == ((9 << 4) | 1))
        *version = _v1;
    else if (ver == ((9 << 4) | 2))
        *version = _v2;

    return mfrc522_error_SUCCESS;
}

uint8_t mfrc522_get_antenna_gain(struct mfrc522_device* dev, mfrc522_antenna_gain_t* gain) {
    mfrc522_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t value = _read_register(dev, RFCfgReg);
    *gain = (mfrc522_antenna_gain_t)((value & 0x7F) >> 4);

    return mfrc522_error_SUCCESS;
}

uint8_t mfrc522_set_antenna_gain(struct mfrc522_device* dev, mfrc522_antenna_gain_t gain) {
    mfrc522_error_t err;
    if ((err = _device_check(dev))) return err;

    uint8_t value = _read_register(dev, RFCfgReg);
    value &= ~(0x07 << 4);
    value |= (gain << 4);
    _write_register(dev, RFCfgReg, value);

    return mfrc522_error_SUCCESS;
}

const char* mfrc522_antenna_gain_str(mfrc522_antenna_gain_t gain) {
    switch (gain) {
        case mfrc522_antenna_gain_18bB_2:
        case mfrc522_antenna_gain_18bB_1: return "18 dB";

        case mfrc522_antenna_gain_23bB_1:
        case mfrc522_antenna_gain_23bB_2: return "23 dB";

        case mfrc522_antenna_gain_33bB: return "33 dB";
        case mfrc522_antenna_gain_38bB: return "38 dB";
        case mfrc522_antenna_gain_43bB: return "43 dB";
        case mfrc522_antenna_gain_48bB: return "48 dB";
        default: break;
    }

    return "undefined";
}

uint8_t mfrc522_is_card_pressent(struct mfrc522_device* dev) {
    mfrc522_error_t err;
    if ((err = _device_check(dev))) return err;

    // TODO: detecting rfid card

    return mfrc522_error_SUCCESS;
}

const char* mfrc522_strerr(mfrc522_error_t err) {
    switch (err) {
        case mfrc522_error_SUCCESS: return "Success";
        case mfrc522_error_NODEV: return "Struct mfrc522_device null";
        case mfrc522_error_NODRV: return "Struct mfrc522_driver null";
        case mfrc522_error_NODRVF: return "Struct mfrc522_driver function null";
        case mfrc522_error_BADPAR: return "Invalid parameter";
        default: break;
    }

    return "undefined";
}